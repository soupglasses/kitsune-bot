import discord
from discord.ext import commands

description = '''A basic bot to try out implementing different tools into.'''

class Bot(commands.Bot):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def add_cog(self, cog: commands.Cog):
        super().add_cog(cog)
        print("Cog loaded:", cog.qualified_name)

    @classmethod
    def create(cls):
        intents = discord.Intents.default()
        intents.typing = False
        intents.presences = False

        return cls(
            command_prefix='.',
            description=description,
            intents=intents,
        )
